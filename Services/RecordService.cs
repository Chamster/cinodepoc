﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CinodePoc.Interfaces;
using CinodePoc.Model.Entities;
using CinodePoc.Model.Utils;
using Microsoft.EntityFrameworkCore;

namespace CinodePoc.Services
{
  public class RecordService : IRecordService
  {
    public RecordService(Context context)
    {
      Context = context;
    }

    private Context Context { get; }

    public async Task<List<Record>> GetAll()
    {
      List<Record> records = await Context.Records.ToListAsync();

      return records;
    }

    public async Task<Guid> SaveRecord(Record record)
    {
      // todo Introduce the logic retaining GUID for updates of existing records.
      try
      {
        await Context.Records.AddAsync(record);
        await Context.SaveChangesAsync();
      }
      catch (Exception) { return Guid.Empty; }

      return record.Id;
    }

    public async Task<bool> DeleteRecord(Guid id)
    {
      Record record = await Context.Records
        .SingleOrDefaultAsync(e => e.Id == id);

      if (record == null)
        return false;

      Context.Remove(record);
      await Context.SaveChangesAsync();

      return true;
    }
  }
}
