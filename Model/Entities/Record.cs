﻿using System;

namespace CinodePoc.Model.Entities
{
  public class Record
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public int Grade { get; set; }
  }
}