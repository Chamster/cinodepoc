﻿using System;
using System.Collections.Generic;
using CinodePoc.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinodePoc.Model.Utils
{
  public class Context : DbContext
  {
    public Context(DbContextOptions<Context> options) { }

    public DbSet<Record> Records { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      base.OnConfiguring(optionsBuilder);

      optionsBuilder.UseInMemoryDatabase("squicker");
      optionsBuilder.ConfigureWarnings(options => options.Throw());
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      EntityTypeBuilder<Record> entity = modelBuilder.Entity<Record>();

      entity.HasKey(e => e.Id);
      entity.Property(e => e.Name).IsRequired();
      entity.Property(e => e.Grade).IsRequired();

      List<Record> records = new List<Record>
      {
        new Record{ Id = Guid.NewGuid(), Name = "JavaScript", Grade = 4 },
        new Record{ Id = Guid.NewGuid(), Name = "HTML", Grade = 3 },
        new Record{ Id = Guid.NewGuid(), Name = "C#", Grade = 5 }
      };
      entity.HasData(records);
    }
  }
}
