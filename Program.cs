using System;
using System.Collections.Generic;
using CinodePoc.Model.Entities;
using CinodePoc.Model.Utils;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace CinodePoc
{
  public class Program
  {
    public static void Main(string[] args)
    {
      IWebHost host = CreateWebHostBuilder(args).Build();

      List<Record> records = new List<Record>
      {
        new Record{ Id = Guid.NewGuid(), Name = "JavaScript", Grade = 3 },
        new Record{ Id = Guid.NewGuid(), Name = "HTML", Grade = 1 },
        new Record{ Id = Guid.NewGuid(), Name = "C#", Grade = 2 }
      };

      using (IServiceScope scope = host.Services.CreateScope())
      {
        IServiceProvider services = scope.ServiceProvider;
        Context context = services.GetRequiredService<Context>();
        context.Records.AddRange(records);
        context.SaveChanges();
      }

      host.Run();
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
  }
}
