﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CinodePoc.Model.Entities;

namespace CinodePoc.Interfaces
{
  public interface IRecordService
  {
    Task<List<Record>> GetAll();
    Task<Guid> SaveRecord(Record record);
    Task<bool> DeleteRecord(Guid id);
  }
}
