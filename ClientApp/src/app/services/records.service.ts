import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Record } from "../models/record";

@Injectable({
  providedIn: "root"
})
export class RecordsService {

  constructor(private http: HttpClient) { }

  private URL = "https://localhost:44318/api/record";

  getAll(): Observable<Record[]> {
    const url = this.URL;

    return this.http.get<Record[]>(url);
  }

  create(record: Record): Observable<string> {
    const url = this.URL;
    const body = record;

    return this.http.post<string>(url, body);
  }

  delete(record: Record): Observable<boolean> {
    const url = this.URL + "/" + record.id;

    return this.http.delete<boolean>(url);
  }
}
