import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Record } from "../../models/record";
import { faSkullCrossbones } from "@fortawesome/free-solid-svg-icons";
import { RecordsService } from "src/app/services/records.service";

@Component({
  selector: "app-setter",
  templateUrl: "./setter.component.html",
  styleUrls: ["setter.component.css"]
})
export class SetterComponent implements OnInit {

  constructor(private service: RecordsService) { }

  @Input() setting = new Record();
  @Output() settingChange = new EventEmitter<Record>();
  @Output() removal = new EventEmitter();

  faRemove = faSkullCrossbones;

  ngOnInit() { }

  onClick() {
    this.service.delete(this.setting)
      .subscribe(
        res => { if (res) this.removal.emit(); },
        err => console.log(err)
      );
  }
}
