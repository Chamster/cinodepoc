import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-grader",
  templateUrl: "./grader.component.html",
  styleUrls: ["grader.component.css"]
})
export class GraderComponent implements OnInit {

  constructor() { }

  @Input() locked: boolean;
  @Input() grade: number;
  @Output() gradeChange = new EventEmitter<number>();

  grades: string[];
  selected: string[];

  private GRADE_COUNT = 5;
  private GRADE_ARRAY = new Array(this.GRADE_COUNT).fill(null)

  ngOnInit() {
    this.grades = this.GRADE_ARRAY
      .map((_, i) => i + 1 + "");
    this.selected = this.GRADE_ARRAY
      .map((_, i) => this.grade === i ? "selected" : "");
  }

  onClick(grade: number) {
    if (this.locked)
      return;

    this.selected = this.selected
      .map((_, i) => grade === i ? "selected" : "");
    this.gradeChange.emit(grade);
  }
}
