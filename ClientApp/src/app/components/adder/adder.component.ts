import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { Record } from "../../models/record";
import { RecordsService } from "src/app/services/records.service";

@Component({
  selector: "app-adder",
  templateUrl: "./adder.component.html",
  styleUrls: ["adder.component.css"]
})
export class AdderComponent implements OnInit {

  constructor(private service: RecordsService) { }

  @Output() addition = new EventEmitter<Record>();

  skill: Record;
  get ability(): string {
    return !this.skill.name
      ? "btn-secondary blocked"
      : "btn-success clickable";
  }

  ngOnInit() {
    this.skill = new Record();
  }

  onClick() {
    if (!this.skill.name)
      return;

    this.service.create(this.skill)
      .subscribe(
        res => {
          if (!res)
            return;

          this.skill.id = res;
          this.addition.emit(this.skill);
          this.skill = new Record();
        },
        err => console.error(err)
      );
  }
}
