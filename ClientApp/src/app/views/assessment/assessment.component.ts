import { Component, OnInit } from "@angular/core";
import { Record } from "../../models/record";
import { RecordsService } from "src/app/services/records.service";

@Component({
  selector: "app-assessment",
  templateUrl: "./assessment.component.html",
  styleUrls: ["./assessment.component.css"]
})
export class AssessmentComponent implements OnInit {

  constructor(private service: RecordsService) { }

  records: Record[];

  ngOnInit() {
    this.service.getAll()
      .subscribe(res => this.records = res);
  }

  onAddition(event: Record) {
    this.records.push(event);
  }

  onRemoval(index: number) {
    this.records.splice(index, 1);
  }
}
