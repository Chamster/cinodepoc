using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CinodePoc.Interfaces;
using CinodePoc.Model.Entities;
using Microsoft.AspNetCore.Mvc;

namespace CinodePoc.Controllers
{
  [Route("api/[controller]")]
  public class RecordController : ControllerBase
  {
    private IRecordService Service { get; }

    public RecordController(IRecordService service)
    {
      Service = service;
    }

    [HttpGet]
    public async Task<ActionResult> GetAll()
    {
      List<Record> records = await Service.GetAll();

      return Ok(records);
    }

    [HttpPost]
    public async Task<ActionResult<Guid>> Create([FromBody]Record record)
    {
      Guid id = await Service.SaveRecord(record);

      if (id == Guid.Empty)
        return BadRequest();

      return Ok(id);
    }

    [HttpPut]
    public ActionResult<bool> Update(Record record)
    {
      throw new NotImplementedException(record.ToString());
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<bool>> Delete(Guid id)
    {
      bool success = await Service.DeleteRecord(id);

      return Ok(success);
    }
  }
}